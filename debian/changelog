libobject-pluggable-perl (1.29-3) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:43:17 +0100

libobject-pluggable-perl (1.29-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 15:09:15 +0100

libobject-pluggable-perl (1.29-2) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Martín Ferrari ]
  * Remove myself from Uploaders.

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Rene Mayorga from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Commit actual contents of 1.29 orig.tar.gz
  * Add spelling.patch
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.5

 -- Florian Schlichting <fsfs@debian.org>  Sat, 07 Jul 2018 21:06:04 +0200

libobject-pluggable-perl (1.29-1) unstable; urgency=low

  * New upstream release. Renamed to Object::Pluggable upstream.
  * Rename Debian package to libobject-pluggable-perl.
  * No longer (build-)depends on libpoe-perl.
  * Build-dep on perl >= 5.10.1 for Pod::Parser 1.36.
  * debian/copyright: Remove information on inc/*; refer to "Debian systems"
    instead of "Debian GNU/Linux systems"; refer to
    /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 21 Oct 2010 14:41:18 +0200

libpoe-component-pluggable-perl (1.26-1) unstable; urgency=low

  [ Ryan Niebur ]
  * Update jawnsy's email address
  * Update ryan52's email address

  [ gregor herrmann ]
  * New upstream release.
  * Convert to source format 3.0 (quilt).
  * debian/copyright: update formatting and copyright info (upstream +
    packaging).
  * Set Standards-Version to 3.9.0 (no changes).
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 Jun 2010 18:45:10 +0200

libpoe-component-pluggable-perl (1.24-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Fix memory leaks (RT#48788)
  * Standards-Version 3.8.3 (no changes)

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

 -- Jonathan Yu <frequency@cpan.org>  Fri, 21 Aug 2009 17:21:01 -0400

libpoe-component-pluggable-perl (1.22-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Jonathan Yu ]
  * New upstream release
  * Updated copyright
    + Upstream copyright years now 2009
    + Updated the Module::Install copyright info
  * Use new shorter rules format
  * Standards-Version 3.8.2 (no changes)
  * Updated control file description
  * Depend on perl (>= 5.6.0-16), per Perl Policy

 -- Jonathan Yu <frequency@cpan.org>  Sun, 26 Jul 2009 17:42:17 -0400

libpoe-component-pluggable-perl (1.20-1) unstable; urgency=low

  * New upstream release
  * update dependencies

 -- Ryan Niebur <ryanryan52@gmail.com>  Wed, 29 Apr 2009 15:39:16 -0700

libpoe-component-pluggable-perl (1.18-1) unstable; urgency=low

  [ Ryan Niebur ]
  * New upstream release
  * Debian Policy 3.8.1

  [ gregor herrmann ]
  * Bump versioned (build) dependency on libpoe-perl as per new upstream
    requirements.

 -- Ryan Niebur <ryanryan52@gmail.com>  Sat, 11 Apr 2009 09:38:46 -0700

libpoe-component-pluggable-perl (1.16-1) unstable; urgency=low

  * New upstream release
  * add myself to uploaders

 -- Ryan Niebur <ryanryan52@gmail.com>  Thu, 05 Mar 2009 07:47:34 -0800

libpoe-component-pluggable-perl (1.14-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * New upstream release
  * debian/control
    + set debhelper version to 7
    + add myself to uploaders
  * debian/rules, debian/compat; updated to dh7 using dh-make-perl
  * debian/copyright:
    + update proposal format
    + add individual copyright stanza for debian/* based
    on debian/changelog
    + mention 3 more people holding copyright on *, according to upstream
    + add copyright stanza for inc/*

 -- Rene Mayorga <rmayorga@debian.org>  Mon, 26 Jan 2009 20:59:46 -0600

libpoe-component-pluggable-perl (1.10-1) unstable; urgency=low

  * New upstream release.

 -- Martín Ferrari <tincho@debian.org>  Wed, 02 Jul 2008 01:53:26 -0300

libpoe-component-pluggable-perl (1.06-2) unstable; urgency=low

  * Re-uploading due to a few errors in debian/control.

 -- Martín Ferrari <tincho@debian.org>  Thu, 12 Jun 2008 22:52:34 -0300

libpoe-component-pluggable-perl (1.06-1) unstable; urgency=low

  * Initial Release (Closes: #484767).

 -- Martín Ferrari <tincho@debian.org>  Fri, 06 Jun 2008 07:54:44 -0300
